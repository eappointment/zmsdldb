<?php

/**
 * @package Dldb
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Dldb\Indexer;

use BO\Dldb\FileAccess;
use OpenSearch\ClientBuilder;
use OpenSearch\Client;

/**
 *
 * Index DLDB data into OpenSearch
 *
 * @suppressWarnings(complexity)
 *
 */
class OpenSearch
{
    public const OS_INDEX_PREFIX = 'dldb-';

    public const OS_INDEX_DATE = 'Ymd-His';

    protected $localeList = array(
        'de',
        'en'
    );

    /**
     * Access to DLDB files
     *
     * @var FileAccess $dldb
     */
    protected $dldb;

    /**
     * hostname for OS
     *
     * @var String $host
     */
    protected $host = 'localhost';

    /**
     * port for OS
     *
     * @var String $port
     */
    protected $port = '9200';

    /**
     * transport method for OS
     *
     * @var String $transport
     */
    protected $transport = 'Http';

    /**
     * The client used to talk to opensearch.
     *
     * @var Client
     */
    protected $client;

    protected $index;

    /**
     * @param $host
     * @param $port
     * @param $transport
     */
    public function __construct($host, $port, $transport)
    {
        $this->setHost($host);
        $this->setPort($port);
        $this->setTransport($transport);
        $this->setClient();
    }

    /**
     * @return $this
     */
    public function run(): OpenSearch
    {
        $this->writeTopics();
        $this->writeServices();
        $this->writeLocations();
        $this->writeAuthorities();
        return $this;
    }

    /**
     *
     * @return OpenSearch
     */
    protected function writeTopics()
    {
        $docs = [];
        $links = [];
        $indexName = $this->getIndexName();
        foreach ($this->dldb->fromTopic()->fetchList() as $topic) {
            $docs[] = [
                'create' => [
                    '_index' => $indexName,
                    '_id' => 'topic-de' . $topic['id'],
                ]
            ];
            $docs[] = [
                'topic' => $topic,
            ];

            if ($topic->isLinked()) {
                $link = [
                    "rank" => 0,
                    "link" => "/" . $topic["path"] . "/",
                    "name" => $topic['name'],
                    "hightlight" => 0,
                    "meta" => [
                        "keywords" => $topic['meta']['keywords'],
                        "titles" => $topic['meta']['titles']
                    ]
                ];
                $links[] = [
                    'create' => [
                        '_index' => $indexName,
                        '_id' => $link['link'],
                    ]
                ];
                $links[] = [
                    'link' => $link,
                ];
                foreach ($topic['links'] as $link) {
                    $links[] = [
                        'create' => [
                            '_index' => $indexName,
                            '_id' => $link['link'],
                        ]
                    ];
                    $links[] = [
                        'link' => $link,
                    ];
                }
            }
        }
        try {
            if (!empty($docs)) {
                $this->client->bulk([
                    'index' => $indexName,
                    'body' => $docs
                ]);
            }
            if (!empty($links)) {
                $this->client->bulk([
                    'index' => $indexName,
                    'body' => $links
                ]);
            }
        } catch (OpenSearchException $e) {
            echo "Fehler beim Hinzufügen der Topics/Links: " . $e->getMessage();
        }
        return $this;
    }

    /**
     *
     * @return self
     */
    protected function writeServices(): OpenSearch
    {
        $indexName = $this->getIndexName();
        $docs = [];
        foreach ($this->localeList as $locale) {
            foreach ($this->dldb->fromService($locale)->fetchList() as $service) {
                $serviceId = 'service_' . $locale . $service['id'];
                $docs[] = [
                    'create' => [
                        '_index' => $indexName,
                        '_id' => $serviceId,
                    ]
                ];
                $docs[] = [
                    'service' => $service,
                ];
            }
        }
        try {
            if (!empty($docs)) {
                $this->client->bulk([
                    'index' => $indexName,
                    'body' => $docs
                ]);
            }
        } catch (OpenSearchException $e) {
            echo "Fehler beim Hinzufügen der Dienstleistungen: " . $e->getMessage();
        }
        return $this;
    }

    /**
     *
     * @return self
     */
    protected function writeLocations(): OpenSearch
    {
        $indexName = $this->getIndexName();
        foreach ($this->localeList as $locale) {
            $docs = [];
            foreach ($this->dldb->fromLocation($locale)->fetchList() as $location) {
                $locationId = 'location_' . $locale . $location['id'];
                $docs[] = [
                    'create' => [
                        '_index' => $indexName,
                        '_id' => $locationId,
                    ]
                ];
                $docs[] = [
                    'location' => $location,
                ];
            }
            try {
                if (!empty($docs)) {
                    $response = $this->client->bulk([
                        'index' => $indexName,
                        'body' => $docs
                    ]);

                    if ($response['errors']) {
                        foreach ($response['items'] as $item) {
                            if (isset($item['index']['error'])) {
                                echo $item['index']['error'];
                            }
                        }
                    }
                }
            } catch (OpenSearchException $e) {
                echo "Fehler beim Hinzufügen der Standorte: " . $e->getMessage();
            }
        }

        return $this;
    }

    /**
     *
     * @return self
     */
    protected function writeAuthorities(): OpenSearch
    {
        $indexName = $this->getIndexName();
        $docs = [];
        foreach ($this->localeList as $locale) {
            foreach ($this->dldb->fromAuthority($locale)->fetchSource() as $authority) {
                $authorityId = 'authority_' . $locale . $authority['id'];
                $docs[] = [
                    'create' => [
                        '_index' => $indexName,
                        '_id' => $authorityId,
                    ]
                ];
                $docs[] = [
                    'authority' => $authority,
                ];
            }
        }
        try {
            if (!empty($docs)) {
                $this->client->bulk([
                    'index' => $indexName,
                    'body' => $docs
                ]);
            }
        } catch (OpenSearchException $e) {
            echo "Fehler beim Hinzufügen der Behörden: " . $e->getMessage();
        }
        return $this;
    }

    /**
     *
     * @return $this
     */
    protected function setClient(): OpenSearch
    {
        if (null === $this->client) {
            $clientBuilder = (new ClientBuilder())
                ->setHosts([
                    [
                        'host' => $this->host,
                        'port' => $this->port,
                        'scheme' => $this->transport
                    ]
                ]);
            $this->client = $clientBuilder->build();
        }
        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param $indexAlias
     *
     * @return array|null
     */
    public function readIndexByAlias($indexAlias): ?array
    {
        $response = $this->client->cat()->aliases(['name' => $indexAlias]);
        if (!empty($response)) {
            foreach ($response as $alias) {
                if ($alias['alias'] === $indexAlias) {
                    return $this->client->indices()->get(['index' => $alias['index']]);
                }
            }
        }
        return null;
    }

    public function setIndex()
    {
        if ($this->index) {
            return;
        }
        $indexName = self::OS_INDEX_PREFIX . date(self::OS_INDEX_DATE);
        $indexBody = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'OpenSearch_Index.json');

        $this->client->indices()->create([
            'index' => $indexName,
            'body' => json_decode($indexBody, true),
        ]);
        $this->index = $this->client->indices()->get(['index' => $indexName]);
        echo "Index $indexName erfolgreich angelegt.\n";
        return $this;
    }

    /**
     *
     * @return self
     */
    public function setHost($host)
    {
        $this->host = $host ?? $this->host;
        return $this;
    }

    /**
     *
     * @return self
     */
    public function setPort($port)
    {
        $this->port = $port ?? $this->port;
        return $this;
    }

    /**
     *
     * @return self
     */
    public function setTransport($transport): OpenSearch
    {
        $this->transport = $transport ?? $this->transport;
        return $this;
    }

    /**
     *
     * @return self
     */
    public function setAlias($aliasName): OpenSearch
    {
        $this->client->indices()->putAlias([
            'index' => array_key_first($this->index),
            'name'  => $aliasName
        ]);
        return $this;
    }

    public function setExportData($dataDir)
    {
        if (is_dir($dataDir)) {
            $this->dldb = new FileAccess();
            $this->dldb->loadFromPath($dataDir);
        } else {
            throw new \Exception("Invalid import parameters for OpenSearch indexer");
        }
    }

    protected function getIndexName(): string
    {
        $currentIndex = $this->getIndex();
        if (! $currentIndex) {
            return '';
        }
        return array_key_first($currentIndex);
    }

    /**
     * Drop all old indice with the prefix OS_INDEX_PREFIX and no alias
     *
     * @param bool $drop
     *
     * @return void
     */
    public function dropOldIndex(bool $drop = false): void
    {
        if (!$drop) {
            return;
        }
        $indexList = $this->client->cat()->indices();
        $currentIndexName = $this->getIndexName();
        foreach ($indexList as $index) {
            $indexName = $index['index'];
            if ($currentIndexName != $indexName && 0 === strpos($indexName, self::OS_INDEX_PREFIX)) {
                $hasAlias = $this->client->indices()->getAlias(['index' => $indexName]);
                if ($hasAlias) {
                    try {
                        $this->client->indices()->delete(['index' => $indexName]);
                        echo "Index $indexName erfolgreich gelöscht.\n";
                    } catch (\Exception $e) {
                        echo "Fehler: " . $e->getMessage() . "\n";
                    }
                }
            }
        }
    }
}
