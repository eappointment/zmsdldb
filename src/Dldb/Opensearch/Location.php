<?php
/**
 * @package ClientDldb
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/
namespace BO\Dldb\Opensearch;

use BO\Dldb\Collection\Authorities;
use BO\Dldb\Entity\Location as Entity;
use BO\Dldb\Collection\Locations as Collection;
use BO\Dldb\File\Location as Base;
use BO\Dldb\Plz\Coordinates;

/**
 * @SuppressWarnings(Coupling)
 */
class Location extends Base
{
    public const FILTER_TYPE = 'location';

    /**
     *
     * @param $itemId
     *
     * @return null|Entity
     */
    public function fetchId($itemId): ?Entity
    {
        if ($itemId == '') {
            return null;
        }
        $query = Helper::boolFilteredQuery(self::FILTER_TYPE, $this->locale);
        $query['bool']['filter'][] = Helper::idsFilter([self::FILTER_TYPE . '_'. $this->locale . $itemId]);
        $response = Helper::readResponse($this->access(), $query);

        if ($response) {
            $source = $response['hits'][0]['_source'][self::FILTER_TYPE];
            return new Entity($source);
        }
        return null;
    }

    /**
     *
     * @param string $serviceCsv
     *
     * @return Collection
     */
    public function fetchList($service_csv = ''): Collection
    {
        $query = Helper::boolFilteredQuery(self::FILTER_TYPE, $this->locale);
        if ($service_csv) {
            $serviceIdList = explode(',', $service_csv);
            $query['bool']['must'][] = [
                'nested' => [
                    'path' => 'location.services',
                    'query' => [
                        'terms' => [
                            'location.services.service' => $serviceIdList
                        ]
                    ]
                ]
            ];
        }
        $response = Helper::readResponse($this->access(), $query);
        $locationList = new Collection();
        if ($response) {
            foreach ($response['hits'] as $hit) {
                $source = $hit['_source'][self::FILTER_TYPE];
                $location = new Entity($source);
                $locationList[$location['id']] = $location;
            }
        }
        return $locationList;
    }

    /**
     *
     * @return Collection
     */
    public function fetchFromCsv($location_csv = ''): Collection
    {
        $query = Helper::boolFilteredQuery(self::FILTER_TYPE, $this->locale);
        $locationList = new Collection();
        if ($location_csv == '') {
            return $locationList;
        }
        $locationIdList = Helper::toIndexedIds($location_csv, self::FILTER_TYPE, $this->locale);
        $query['bool']['filter'][] = Helper::idsFilter($locationIdList);
        $response = Helper::readResponse($this->access(), $query);
        if ($response) {
            foreach ($response['hits'] as $hit) {
                $source = $hit['_source'][self::FILTER_TYPE];
                $location = new Entity($source);
                $locationList[$location['id']] = $location;
            }
        }
        return $locationList;
    }

    /**
     *
     * @param        $querystring
     * @param string $serviceCsv
     *
     * @return Authorities
     */
    public function searchAll($querystring, string $serviceCsv = ''): Authorities
    {
        $sort = self::FILTER_TYPE;
        $sortAuthorities = true;
        $query = Helper::boolFilteredQuery(self::FILTER_TYPE, $this->locale);
        if ($querystring > 10000 && $querystring < 15000) {
            $limit = 10;
            $serviceIdList = explode(',', $serviceCsv);
            $query['bool']['must'][] = [
                'query_string' => [
                    'query' => '*',
                    'fields' => [
                        'location.address.postal_code^9'
                    ],
                ]
            ];

            $coordinates = Coordinates::zip2LatLon($querystring);
            if (false !== $coordinates) {
                $sortAuthorities = false;
                $sort = [
                    [
                        '_geo_distance' => [
                            'location.geo' => [
                                'lat' => $coordinates['lat'],
                                'lon' => $coordinates['lon'],
                            ],
                            'order' => 'asc',
                            'unit' => 'km',
                            'distance_type' => 'plane'
                        ],
                    ],
                ];
            }
        } else {
            $limit = 500;
            $querystring = empty(trim($querystring)) ? '*' : str_replace('/', '\/', $querystring);
            if (strlen($querystring) < 3) {
                return new Authorities();
            }
            $serviceIdList = explode(',', $serviceCsv);
            $query['bool']['must'][] = [
                'query_string' => [
                    'query' => $querystring,
                    'fields' => [
                        'location.name^9',
                        'location.authority.name^5',
                        'location.address.street',
                    ],
                    'default_operator' => 'AND',
                    'analyze_wildcard' => true,
                    'type' => 'phrase_prefix',
                ]
            ];
        }

        $query['bool']['must'][] = [
            'nested' => [
                'path' => 'location.services',
                'query' => [
                    'terms' => [
                        'location.services.service' => $serviceIdList
                    ]
                ]
            ]
        ];

        $response = Helper::readResponse($this->access(), $query, $sort, $limit);
        $locationList = new Collection();
        if ($response) {
            foreach ($response['hits'] as $hit) {
                $source = $hit['_source'][self::FILTER_TYPE];
                $location = new Entity($source);
                $locationList[$location['id']] = $location;
            }
        }
        $authorityList = $this->access()
            ->fromAuthority()
            ->fromLocationResults($locationList);

        return ($sortAuthorities) ? $authorityList->sortByName(): $authorityList;
    }
}
