<?php
namespace BO\Dldb\Opensearch;

class Helper
{
    public static $sourceData = [];

    public static function boolFilteredQuery($type, $locale): array
    {
        return [
            'bool' => [
                'must' => [
                    [
                        'term' => [
                            $type . '.meta.locale' => [
                                'value' => $locale
                            ]
                        ]
                    ]
                ],
                'filter' => [
                    [
                        'exists' => [
                            'field' => $type
                        ]
                    ]
                ]
            ]
        ];
    }

    public static function idsFilter($ids): array
    {
        return [
            'ids' => [
                'values' => $ids
            ]
        ];
    }

    public static function toIndexedIds($csvString, $type, $locale)
    {
        $list = [];
        $idList = explode(',', $csvString);
        foreach ($idList as $itemId) {
            $list[] = $type . '_' . $locale . $itemId;
        }
        return $list;
    }

    /**
     * @param $type
     *
     * @return array[]
     */
    public static function sortNamesByType($type): array
    {
        return [
            $type.'.name.sort' => [
                'order' => 'asc'
            ]
        ];
    }

    public static function readResponse($access, $query, $sortType = null, $limit = 10000)
    {
        $sort = [];
        if (is_string($sortType) && $sortType != '') {
            $sort = Helper::sortNamesByType($sortType);
        }
        if (is_array($sortType) && count($sortType) > 0) {
            $sort = $sortType;
        }
        $params = [
            'index' => array_key_first($access->getIndex()),
            'body'  => [
                'query' => $query,
                'sort' => $sort,
            ],
            'size' => $limit
        ];
        if (count(self::$sourceData)) {
            $params['body']['_source'] = self::$sourceData;
            self::$sourceData = [];
        }
        $response = $access->getClient()->search($params);
        if ($response['hits']['total']['value'] > 0) {
            return $response['hits'];
        }
        return null;
    }
}
