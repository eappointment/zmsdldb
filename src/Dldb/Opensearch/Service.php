<?php
/**
 * @package ClientDldb
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/
namespace BO\Dldb\Opensearch;

use BO\Dldb\Entity\Service as Entity;
use BO\Dldb\Collection\Services as Collection;
use BO\Dldb\File\Service as Base;

/**
 * @SuppressWarnings(Coupling)
 */
class Service extends Base
{
    public const FILTER_TYPE = 'service';

    public function fetchId($itemId): ?Entity
    {
        $query = Helper::boolFilteredQuery(self::FILTER_TYPE, $this->locale);
        $query['bool']['filter'][] = Helper::idsFilter([self::FILTER_TYPE . '_' . $this->locale . $itemId]);
        $response = Helper::readResponse($this->access(), $query);
        if ($response) {
            $source = $response['hits'][0]['_source'][self::FILTER_TYPE];
            return new Entity($source);
        }
        return null;
    }

    /**
     *
     * @param string $location_csv
     *
     * @return Collection
     */
    public function fetchList($location_csv = ''): Collection
    {
        $query = Helper::boolFilteredQuery(self::FILTER_TYPE, $this->locale);
        if ($location_csv) {
            $locationIdList = explode(',', $location_csv);
            $query['bool']['must'][] = [
                'nested' => [
                    'path' => 'service.locations',
                    'query' => [
                        'terms' => [
                            'service.locations.location' => $locationIdList
                        ]
                    ]
                ]
            ];
        }
        $response = Helper::readResponse($this->access(), $query, self::FILTER_TYPE);
        $serviceList = new Collection();
        if ($response) {
            foreach ($response['hits'] as $hit) {
                $source = $hit['_source'][self::FILTER_TYPE];
                $service = new Entity($source);
                $serviceList[$service['id']] = $service;
            }
        }
        return $serviceList;
    }

    /**
     *
     * @return Collection
     */
    public function fetchFromCsv($service_csv = ''): Collection
    {
        $query = Helper::boolFilteredQuery(self::FILTER_TYPE, $this->locale);
        $serviceList = new Collection();
        if ($service_csv == '') {
            return $serviceList;
        }
        $serviceIdList = Helper::toIndexedIds($service_csv, self::FILTER_TYPE, $this->locale);
        $query['bool']['filter'][] = Helper::idsFilter($serviceIdList);
        $response = Helper::readResponse($this->access(), $query);
        if ($response) {
            foreach ($response['hits'] as $hit) {
                $source = $hit['_source'][self::FILTER_TYPE];
                $service = new Entity($source);
                $serviceList[$service['id']] = $service;
            }
        }
        return $serviceList;
    }

    /**
     *
     * @return Collection\Services
     */
    public function searchAll($querystring, string $service_csv = '', string $location_csv = '')
    {
        $query = Helper::boolFilteredQuery(self::FILTER_TYPE, $this->locale);
        $locationsCsvByUser = false;
        if (! $location_csv) {
            $location_csv = $this->fetchLocationCsv($service_csv);
        } else {
            $locationsCsvByUser = true;
        }
        $querystring = empty(trim($querystring)) ? '*' : str_replace('/', '\/', $querystring);
        $locationIdList = explode(',', $location_csv);
        $isAzQuery = strpos($querystring, 'az:') !== false;

        if ($isAzQuery) {
            $azValue = substr($querystring, 3);
            $query['bool']['must'][] = [
                'prefix' => [
                    'service.name.az' => [
                        'value' => $azValue,
                    ]
                ]
            ];
        } else {
            if (strlen($querystring) < 3) {
                return new Collection();
            }
            $query['bool']['should'][] = [
                'match' => [
                    'service.name' => [
                        'query' => $querystring,
                        'analyzer' => 'query_analyzer',
                        'fuzziness' => 'AUTO', // Automatische Toleranz
                        'operator' => 'and',
                        'boost' => 1.5
                    ]
                ]
            ];

            $query['bool']['should'][] = [
                'match' => [
                    'service.meta.keywords' => [
                        'query' => $querystring,
                        'analyzer' => 'query_analyzer',
                        'fuzziness' => 'AUTO',
                        'operator' => 'and',
                        'boost' => 1
                    ]
                ]
            ];

            $query['bool']['should'][] = [
                'query_string' => [
                    'query' => '*' . strtolower($querystring) . '*', // Teilstring-Suche
                    'fields' => ['service.name', 'service.meta.keywords'],
                    'analyze_wildcard' => true, // Wildcard aktivieren
                    'boost' => 1
                ]
            ];

            $query['bool']['minimum_should_match'] = 1;
        }

        $query['bool']['must'][] = [
            'nested' => [
                'path' => 'service.locations',
                'query' => [
                    'terms' => [
                        'service.locations.location' => $locationIdList
                    ]
                ]
            ]
        ];

        $serviceList = new Collection();
        $response = Helper::readResponse($this->access(), $query, self::FILTER_TYPE, 500);
        if ($response) {
            foreach ($response['hits'] as $hit) {
                $source = $hit['_source'][self::FILTER_TYPE];
                $service = new Entity($source);
                $serviceList[$service['id']] = $service;
            }
        }
        if ($locationsCsvByUser) {
            $serviceList = $serviceList->containsLocation($location_csv);
        }
        return $serviceList;
    }
}
