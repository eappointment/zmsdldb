<?php
/**
 * @package ClientDldb
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/
namespace BO\Dldb\Opensearch;

use \BO\Dldb\Entity\Authority as Entity;
use \BO\Dldb\Entity\Location as LocationEntity;
use \BO\Dldb\Collection\Authorities as Collection;
use \BO\Dldb\File\Authority as Base;

/**
 */
class Authority extends Base
{
    public const FILTER_TYPE = 'location';

    /**
     *
     * @param array $serviceIdList
     *
     * @return Collection
     */
    public function fetchList($servicelist = []): Collection
    {
        $query = Helper::boolFilteredQuery(self::FILTER_TYPE, $this->locale);
        if (count($servicelist) > 0) {
            $query['bool']['must'][] = [
                'nested' => [
                    'path' => 'location.services',
                    'query' => [
                        'terms' => [
                            'location.services.service' => $servicelist
                        ]
                    ]
                ]
            ];
        }
        $response = Helper::readResponse($this->access(), $query, self::FILTER_TYPE, 1000);
        $locationList = new Collection();
        if ($response) {
            foreach ($response['hits'] as $hit) {
                $source = $hit['_source'][self::FILTER_TYPE];
                $location = new Entity($source);
                $locationList[$location['id']] = $location;
            }
        }
        return $this->fromLocationResults($locationList);
    }


    /**
     * Take an opensearch result and return an authority list
     *
     * @param $resultList
     *
     * @return Collection
     */
    public function fromLocationResults($resultList): Collection
    {
        $authorityList = new Collection();
        foreach ($resultList as $location) {
            $entity = new LocationEntity($location);
            $authorityList->addLocation($entity);
        }
        return $authorityList;
    }
}
