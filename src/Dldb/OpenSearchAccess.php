<?php
/**
 * @package 115Mandant
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Dldb;

class OpenSearchAccess extends FileAccess
{
    /**
     * The client used to talk to opensearch.
     *
     * @var Client;
     */
    protected $client;

    /**
     * @var array|null
     */
    protected $index = null;

    /**
     *
     * @param null   $indexAlias
     * @param string $host
     * @param string $port
     * @param string $transport
     *
     * @throws Exception
     */
    public function __construct($indexAlias = null, $host = 'localhost', $port = '9200', $transport = 'Http')
    {
        $openSearchRepository = new Indexer\OpenSearch($host, $port, $transport);
        $this->client = $openSearchRepository->getClient();
        if ($indexAlias) {
            $this->index = $openSearchRepository->readIndexByAlias($indexAlias);
        }
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getIndex(): ?array
    {
        return $this->index;
    }

    public function getIndexName(): string
    {
        return ($this->index) ? array_key_first($this->index) : '';
    }

    /**
     *
     * @param        $locationJson
     * @param string $locale
     *
     * @return self
     */
    public function loadLocations($locationJson, $locale = 'de'): OpenSearchAccess
    {
        $this->accessInstance[$locale]['Location'] = new Opensearch\Location($locationJson, $locale);
        $this->accessInstance[$locale]['Location']->setAccessInstance($this);
        return $this;
    }

    /**
     *
     * @return self
     */
    public function loadServices($serviceJson, $locale = 'de'): OpenSearchAccess
    {
        $this->accessInstance[$locale]['Service'] = new Opensearch\Service($serviceJson, $locale);
        $this->accessInstance[$locale]['Service']->setAccessInstance($this);
        return $this;
    }

    /**
     *
     * @return self
     */
    public function loadTopics($topicJson, $locale = 'de'): OpenSearchAccess
    {
        $this->accessInstance[$locale]['Topic'] = new Opensearch\Topic($topicJson, $locale);
        $this->accessInstance[$locale]['Topic']->setAccessInstance($this);
        $this->accessInstance[$locale]['Link'] = new Opensearch\Link($topicJson, $locale);
        $this->accessInstance[$locale]['Link']->setAccessInstance($this);
        return $this;
    }

    /**
     *
     * @return self
     */
    public function loadSettings($settingsJson): OpenSearchAccess
    {
        $this->accessInstance['de']['Setting'] = new Opensearch\Setting($settingsJson);
        $this->accessInstance['de']['Setting']->setAccessInstance($this);
        $this->accessInstance['de']['Office'] = new Opensearch\Office($settingsJson);
        $this->accessInstance['de']['Office']->setAccessInstance($this);
        $this->accessInstance['de']['Borough'] = new Opensearch\Borough($settingsJson);
        $this->accessInstance['de']['Borough']->setAccessInstance($this);
        return $this;
    }

  
    /**
     *
     * @return self
     */
    public function loadAuthorities($authorityJson, $locale = 'de'): OpenSearchAccess
    {
        $this->accessInstance[$locale]['Authority'] = new Opensearch\Authority($authorityJson, $locale);
        $this->accessInstance[$locale]['Authority']->setAccessInstance($this);
        return $this;
    }
}
