## 2.27.01
* #61261 - nategood/httpful has been replaced by the guzzle http client

## 2.27.0
* Compatibility with PHP80 established and set as minimum requirement for composer
* Introduction of Opensearch as an additional search engine